# Terraform
Pipeline que abstrae el flujo común para proyectos que usen terraform como herramienta de IaC.<br>
Guarda el state en el propio gitlab.

## Getting started
Basta con colocar en el archivo `.gitlab-ci.yml` de tu proyecto el siguiente fragmento:

```yaml
include:
  - project: hexamples/templates/terraform
    file: default.gitlab-ci.yml
    ref: main
```

## Personalización

### Terraform version
Se puede modificar la versión de terraform a usar con la siguiente variable.
```yaml
variables:
  TERRAFORM_VERSION: 1.3.5
```

O cambiar por completo la imagen docker a usar para ejecutar los jobs con la variable:
```yaml
variables:
  TERRAFORM_DOCKER_IMAGE: hashicorp/terraform:1.3.5
```

## Extras
Existen las variables:
```yaml
variables:
  TF_VAR_remote_state_default_http_username: gitlab-ci-token
  TF_VAR_remote_state_default_http_password: ${CI_JOB_TOKEN}
```

Que facilitan el acceso a states remotos si están en gitlab y se tiene los permisos necesarios para leerlos. Ejemplo:
```terraform
data "terraform_remote_state" "networking" {
  backend = "http"
  config = {
    username = var.remote_state_default_http_username
    password = var.remote_state_default_http_password
    address  = ...
  }
}
```

Sin embargo debes crear las variables para que sean tomadas en cuenta:
```terraform
variable "remote_state_default_http_username" {}
variable "remote_state_default_http_password" {}
```
